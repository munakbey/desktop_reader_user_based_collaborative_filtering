﻿namespace yazlab1
{
	partial class Giris
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Giris));
			this.label1 = new System.Windows.Forms.Label();
			this.kA = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.sifre = new System.Windows.Forms.Label();
			this.btn_Giris = new System.Windows.Forms.Button();
			this.uyeOl = new System.Windows.Forms.LinkLabel();
			this.adminGiris = new System.Windows.Forms.LinkLabel();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Arial Unicode MS", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
			this.label1.Location = new System.Drawing.Point(229, 41);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(128, 46);
			this.label1.TabIndex = 0;
			this.label1.Text = "LOGIN";
			// 
			// kA
			// 
			this.kA.Location = new System.Drawing.Point(376, 143);
			this.kA.Name = "kA";
			this.kA.Size = new System.Drawing.Size(100, 20);
			this.kA.TabIndex = 1;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(376, 187);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(100, 20);
			this.textBox2.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
			this.label2.Location = new System.Drawing.Point(296, 144);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(65, 15);
			this.label2.TabIndex = 3;
			this.label2.Text = "Username";
			// 
			// sifre
			// 
			this.sifre.AutoSize = true;
			this.sifre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
			this.sifre.Location = new System.Drawing.Point(296, 188);
			this.sifre.Name = "sifre";
			this.sifre.Size = new System.Drawing.Size(61, 15);
			this.sifre.TabIndex = 4;
			this.sifre.Text = "Password";
			// 
			// btn_Giris
			// 
			this.btn_Giris.Location = new System.Drawing.Point(390, 231);
			this.btn_Giris.Name = "btn_Giris";
			this.btn_Giris.Size = new System.Drawing.Size(75, 23);
			this.btn_Giris.TabIndex = 5;
			this.btn_Giris.Text = "Login";
			this.btn_Giris.UseVisualStyleBackColor = true;
			this.btn_Giris.Click += new System.EventHandler(this.btn_Giris_Click);
			// 
			// uyeOl
			// 
			this.uyeOl.AutoSize = true;
			this.uyeOl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
			this.uyeOl.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
			this.uyeOl.Location = new System.Drawing.Point(448, 268);
			this.uyeOl.Name = "uyeOl";
			this.uyeOl.Size = new System.Drawing.Size(43, 15);
			this.uyeOl.TabIndex = 6;
			this.uyeOl.TabStop = true;
			this.uyeOl.Text = "Uye Ol";
			this.uyeOl.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.uyeOl_LinkClicked);
			// 
			// adminGiris
			// 
			this.adminGiris.AutoSize = true;
			this.adminGiris.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
			this.adminGiris.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
			this.adminGiris.Location = new System.Drawing.Point(323, 268);
			this.adminGiris.Name = "adminGiris";
			this.adminGiris.Size = new System.Drawing.Size(73, 15);
			this.adminGiris.TabIndex = 7;
			this.adminGiris.TabStop = true;
			this.adminGiris.Text = "Admin Girisi";
			this.adminGiris.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.adminGiris_LinkClicked);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(30, 112);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(239, 194);
			this.pictureBox1.TabIndex = 8;
			this.pictureBox1.TabStop = false;
			// 
			// Giris
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(585, 437);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.adminGiris);
			this.Controls.Add(this.uyeOl);
			this.Controls.Add(this.btn_Giris);
			this.Controls.Add(this.sifre);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.kA);
			this.Controls.Add(this.label1);
			this.Name = "Giris";
			this.Text = "Giris";
			this.Load += new System.EventHandler(this.Giris_Load);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox kA;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label sifre;
		private System.Windows.Forms.Button btn_Giris;
		private System.Windows.Forms.LinkLabel uyeOl;
		private System.Windows.Forms.LinkLabel adminGiris;
		private System.Windows.Forms.PictureBox pictureBox1;
	}
}