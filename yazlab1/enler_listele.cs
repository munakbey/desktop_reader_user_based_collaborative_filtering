﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace yazlab1
{
	
	public partial class enler_listele : Form
	{
		public static string cnn = ConfigurationManager.ConnectionStrings["MySql"].ToString();
		public static MySqlConnection baglan = new MySqlConnection(cnn);


		public ArrayList kitapAdi = new ArrayList();
		public ArrayList yazar = new ArrayList();
		public ArrayList ısbn_gecici = new ArrayList();

		public static string pdf;//gosterilecek pdf in yolunu tutar.
		public static string ısbn;//gosterilecek kitabın ısbnsini tutar.

		public enler_listele(){
			InitializeComponent();
	
		}

		private void Öneri_Load(object sender, EventArgs e){
			
		}

		private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e){

		}
		public void listele(String sorgu){
			
			try{
				baglan.Open();


				MySqlCommand MyCommand2 = new MySqlCommand(sorgu, baglan);
				MySqlDataReader Rs = MyCommand2.ExecuteReader();

				while (Rs.Read()){
					ısbn_gecici.Add(Rs["ISBN"].ToString());

				}


				baglan.Close();
			}
			catch (MySqlException exc){
				MessageBox.Show("Veri Tabanına Baglanamadı!! ");
				throw;
			}

			doldurArrayList();

			gosterListe();


		}

		public void doldurArrayList()
		{
			try
			{
				for (int i = 0; i < ısbn_gecici.Count; i++)
				{
					baglan.Open();

					String sorgu = "SELECT * FROM yazlab.`bx-books` where  ISBN='" + ısbn_gecici[i] + "' limit 1";
					MySqlCommand MyCommand2 = new MySqlCommand(sorgu, baglan);
					MySqlDataReader Rs = MyCommand2.ExecuteReader();

					while (Rs.Read())
					{
						yazar.Add(Rs["BookAuthor"].ToString());
						kitapAdi.Add(Rs["BookTitle"].ToString());
			//						MessageBox.Show(Rs["BookAuthor"].ToString());
					}

					baglan.Close();
				}
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("doldurArrayList -BAGLANTI HATASI!");
			}
		}



		public void gosterListe(){

			dataGridView1.ColumnCount = 2;
			dataGridView1.Columns[0].Name = "Kitap Adı";
			dataGridView1.Columns[1].Name = "Yazar";

			dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

			for (int i = 0; i < kitapAdi.Count; i++){
			
				dataGridView1.Rows.Add(kitapAdi[i], yazar[i]);
			}
		}

		private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e){

			string tmp = getID_Kitap(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
			MessageBox.Show(tmp + "!!");//ısbn
			string gonder = getPdf(tmp);

			//getPdf(tmp);

			Pdf goster = new Pdf(1,1,gonder);
			
		//	goster.getAdres(gonder);
			goster.Show();

			int kullanici = Sayfalama.getUserID();
			okunanaEkle(kullanici,tmp);


		}

		public string getID_Kitap(string kitapAdi){//Kitap adından pdf ine ulasmak için ISBN ye geçiş.Ratingdeki kitapların hepsi booksda olmadığından ArrayListten direkt çekilmedi.
			
			try{
				baglan.Open();
				String sorgu = "select ISBN from `bx-books` where BookTitle=?";
				MySqlCommand komut = new MySqlCommand(sorgu, baglan);
				komut.Parameters.Add("?BookTitle", kitapAdi);
				MySqlDataReader Rs = komut.ExecuteReader();

				while (Rs.Read()){
					ısbn=Rs["ISBN"].ToString();
				}
				baglan.Close();

				
			}
			catch (MySqlException ex){
				MessageBox.Show("Baglanti Hatasi");
			}

			return ısbn;
		}

		public string getPdf(string ısbn){
			try{

				baglan.Open();
				String sorgu = "select pdf from `bx-pdf` where ISBN=?";
				MySqlCommand komut = new MySqlCommand(sorgu, baglan);
				komut.Parameters.Add("?ISBN", ısbn);
				MySqlDataReader Rs = komut.ExecuteReader();

				while (Rs.Read()){
					pdf = (Rs["pdf"].ToString());
				}
				baglan.Close();


			}
			catch (MySqlException ex){
				MessageBox.Show("getPdf-Baglanti Hatasi");
			}
			return pdf;
		}

		public static void okunanaEkle(int uid,string ısbn_){
			try{
				baglan.Open();

				String sorgu = "INSERT INTO `bxbookratings` (`UserID`, `ISBN`) SELECT " + uid + ",'" + ısbn_ + "' FROM dual WHERE NOT EXISTS(select UserID, ISBN from bxbookratings where UserID = " + uid + " and ISBN = '" + ısbn_ + "')";
				MySqlCommand MyCommand2 = new MySqlCommand(sorgu, baglan);
				
				MySqlDataReader MyReader2;


				MyReader2 = MyCommand2.ExecuteReader();

				baglan.Close();
			}
			catch (MySqlException ex){
				MessageBox.Show("Can not open connection ! ");
				throw;
			}
		}

	}
}
