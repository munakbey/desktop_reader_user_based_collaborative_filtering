﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace yazlab1
{
	public partial class KitapEkle : Form
	{

		public static string cnn = ConfigurationManager.ConnectionStrings["MySql"].ToString();
		public MySqlConnection baglanti = new MySqlConnection(cnn);

		public KitapEkle()
		{
			InitializeComponent();
		}

		private void ISBN_Click(object sender, EventArgs e)
		{

		}

		private void KitapEkle_Load(object sender, EventArgs e)
		{

		}

	
		private void listele_Click_1(object sender, EventArgs e)
		{
			listeleKitap();
		}

		private void Klistele_Click_1(object sender, EventArgs e)
		{
			kisiListele();

		}

		private void ksil_Click_1(object sender, EventArgs e)
		{
			baglanti.Open();
			string sorgu = "DELETE FROM `bx-users` WHERE userID=@UserID";
			MySqlCommand sorgu_ = new MySqlCommand(sorgu, baglanti);
			sorgu_.Connection = baglanti;
			sorgu_.Parameters.AddWithValue("@userID", dataGridView1.CurrentRow.Cells[0].Value.ToString());
			sorgu_.ExecuteNonQuery();
			MessageBox.Show("SEÇİLİ KULLANICI SİLİNDİ!!!");
			baglanti.Close();
			kisiListele();
		}

		private void silbuton_Click_1(object sender, EventArgs e)
		{
			baglanti.Open();
			string sorgu = "DELETE FROM `bx-books` WHERE ISBN=@ISBN";
			MySqlCommand sorgu_ = new MySqlCommand(sorgu, baglanti);
			sorgu_.Connection = baglanti;
			sorgu_.Parameters.AddWithValue("@ISBN", dataGridView1.CurrentRow.Cells[0].Value.ToString());
			sorgu_.ExecuteNonQuery();
			MessageBox.Show("SEÇİLİ KİTAP SİLİNDİ!!!");
			baglanti.Close();
			listeleKitap();
		}

		private void button1_Click_1(object sender, EventArgs e)
		{
			try
			{
				if (baglanti.State == ConnectionState.Closed)

					baglanti.Open();


				if (textBox1.TextLength != 0 && textBox2.TextLength != 0 && textBox3.TextLength != 0 && textBox4.TextLength != 0 && textBox5.TextLength != 0 && textBox6.TextLength != 0 && textBox7.TextLength != 0 && textBox8.TextLength != 0 && textBox9.TextLength!=0)
				{
					string query = "insert into `bx-books` values (?,?,?,?,?,?,?,?)";
					
					MySqlCommand veri = new MySqlCommand(query, baglanti);
					
					veri.Parameters.AddWithValue("?ISBN", textBox1.Text);
					veri.Parameters.AddWithValue("?BookTitle", textBox2.Text);
					veri.Parameters.AddWithValue("?BookAuthor", textBox3.Text);
					veri.Parameters.AddWithValue("?YearOfPublication", textBox4.Text);
					veri.Parameters.AddWithValue("?Publisher", textBox5.Text);
					veri.Parameters.AddWithValue("?ImageURLS", textBox6.Text);
					veri.Parameters.AddWithValue("?ImageURLM", textBox7.Text);
					veri.Parameters.AddWithValue("?ImageURLL", textBox8.Text);
					
					MySqlDataReader MyReader2;
					MyReader2 = veri.ExecuteReader();
					baglanti.Close();
					baglanti.Open();
					string query1 = "insert into `bx-pdf` values(?,?) ";
					MySqlCommand aa = new MySqlCommand(query1, baglanti);
					aa.Parameters.AddWithValue("?ISBN", textBox1.Text);
					aa.Parameters.AddWithValue("?pdf", textBox9.Text);
					
					MySqlDataReader MyReader3;
					
					MyReader3 = aa.ExecuteReader();
					baglanti.Close();
					MessageBox.Show("işlemm başarılı");

					listeleKitap();

				}
				else
				{
					MessageBox.Show("Eksiksiz Giriş Yapınız!!!");
					
				}
				


			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		
		}

		public void kisiListele()
		{
			try
			{
				baglanti.Open();
				string sorgu = "SELECT * FROM `bx-users`";
				MySqlCommand sorgu_ = new MySqlCommand(sorgu, baglanti);

				MySqlDataAdapter mda = new MySqlDataAdapter();
				mda.SelectCommand = sorgu_;

				DataTable dt = new DataTable();
				mda.Fill(dt);

				dataGridView1.DataSource = dt;

				baglanti.Close();
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Veri Tabanına Baglanamadı! ");
				throw;
			}
		}

		public void listeleKitap()
		{
			try
			{
				baglanti.Open();
				string sorgu = "SELECT * FROM `bx-books`";
				MySqlCommand sorgu_ = new MySqlCommand(sorgu, baglanti);

				MySqlDataAdapter mda = new MySqlDataAdapter();
				mda.SelectCommand = sorgu_;

				DataTable dt = new DataTable();
				mda.Fill(dt);

				dataGridView1.DataSource = dt;

				baglanti.Close();
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Veri Tabanına Baglanamadı! ");
				throw;
			}
		}
		
		private void button2_Click(object sender, EventArgs e)
		{
			OpenFileDialog opd = new OpenFileDialog();
			if (opd.ShowDialog() == DialogResult.OK)
			{
				textBox6.Text = opd.FileName;
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			OpenFileDialog opd = new OpenFileDialog();
			if (opd.ShowDialog() == DialogResult.OK)
			{
				textBox9.Text = opd.FileName;
			}
		}
	}
}

