﻿namespace yazlab1
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.textBox5 = new System.Windows.Forms.TextBox();
			this.userID = new System.Windows.Forms.Label();
			this.location = new System.Windows.Forms.Label();
			this.age = new System.Windows.Forms.Label();
			this.username = new System.Windows.Forms.Label();
			this.password = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(417, 65);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(100, 20);
			this.textBox1.TabIndex = 0;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(417, 102);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(100, 20);
			this.textBox2.TabIndex = 1;
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(417, 139);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(100, 20);
			this.textBox3.TabIndex = 2;
			// 
			// textBox4
			// 
			this.textBox4.Location = new System.Drawing.Point(417, 181);
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new System.Drawing.Size(100, 20);
			this.textBox4.TabIndex = 3;
			// 
			// textBox5
			// 
			this.textBox5.Location = new System.Drawing.Point(417, 223);
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new System.Drawing.Size(100, 20);
			this.textBox5.TabIndex = 4;
			// 
			// userID
			// 
			this.userID.AutoSize = true;
			this.userID.Location = new System.Drawing.Point(351, 72);
			this.userID.Name = "userID";
			this.userID.Size = new System.Drawing.Size(38, 13);
			this.userID.TabIndex = 5;
			this.userID.Text = "userID";
			// 
			// location
			// 
			this.location.AutoSize = true;
			this.location.Location = new System.Drawing.Point(351, 109);
			this.location.Name = "location";
			this.location.Size = new System.Drawing.Size(44, 13);
			this.location.TabIndex = 6;
			this.location.Text = "location";
			// 
			// age
			// 
			this.age.AutoSize = true;
			this.age.Location = new System.Drawing.Point(351, 146);
			this.age.Name = "age";
			this.age.Size = new System.Drawing.Size(25, 13);
			this.age.TabIndex = 7;
			this.age.Text = "age";
			// 
			// username
			// 
			this.username.AutoSize = true;
			this.username.Location = new System.Drawing.Point(351, 188);
			this.username.Name = "username";
			this.username.Size = new System.Drawing.Size(53, 13);
			this.username.TabIndex = 8;
			this.username.Text = "username";
			this.username.Click += new System.EventHandler(this.label4_Click);
			// 
			// password
			// 
			this.password.AutoSize = true;
			this.password.Location = new System.Drawing.Point(351, 230);
			this.password.Name = "password";
			this.password.Size = new System.Drawing.Size(52, 13);
			this.password.TabIndex = 9;
			this.password.Text = "password";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(417, 266);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(100, 23);
			this.button2.TabIndex = 11;
			this.button2.Text = "KAYDET";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(639, 375);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.password);
			this.Controls.Add(this.username);
			this.Controls.Add(this.age);
			this.Controls.Add(this.location);
			this.Controls.Add(this.userID);
			this.Controls.Add(this.textBox5);
			this.Controls.Add(this.textBox4);
			this.Controls.Add(this.textBox3);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.textBox1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.TextBox textBox5;
		private System.Windows.Forms.Label userID;
		private System.Windows.Forms.Label location;
		private System.Windows.Forms.Label age;
		private System.Windows.Forms.Label username;
		private System.Windows.Forms.Label password;
		private System.Windows.Forms.Button button2;
	}
}

