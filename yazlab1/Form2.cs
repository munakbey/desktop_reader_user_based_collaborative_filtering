﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace yazlab1
{
	public partial class Form2 : Form
	{
		public static string cnn = ConfigurationManager.ConnectionStrings["MySql"].ToString();
		public MySqlConnection baglan = new MySqlConnection(cnn);
	//	DataGridViewComboBoxColumn combo = new DataGridViewComboBoxColumn();
		int sayac = 0;
		public static ArrayList tutISBN = new ArrayList();
		public static ArrayList tutpuan = new ArrayList();



		public Form2()
		{
			InitializeComponent();
			this.dataGridView1.Columns[0].Visible = false;
			gosterVeri();
			//	ekleCombo();

	//		kontrol();
	//		ekleDB();


		}

		private void Form2_Load(object sender, EventArgs e)
		{

		}
		public void gosterVeri()
		{
			try
			{
				baglan.Open();
//				MessageBox.Show("Connection Open ! ");
				MySqlCommand query = new MySqlCommand("SELECT ISBN,BookTitle,BookAuthor,YearOfPublication,Publisher FROM `bx-books` ", baglan);
			
				MySqlDataAdapter mda = new MySqlDataAdapter();
					mda.SelectCommand = query;
		
				DataTable dt = new DataTable();
				dt.Columns.Add("Secili", System.Type.GetType("System.Boolean"));
				mda.Fill(dt);
				dataGridView1.DataSource = dt;

				baglan.Close();
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Can not open connection ! ");
				throw;
			}
		}

		private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e) //Ustune tıklandıgında ISBN yı gosterme
		{
			dataGridView1.CurrentRow.Selected = true;
			String k=dataGridView1.Rows[e.RowIndex].Cells["ISBN"].FormattedValue.ToString();
		//		MessageBox.Show(k);
		}


		private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e){ //Kac satır secildiği-checkbox- ile
			
			/*for (int i = 0; i <dataGridView1.Rows.Count; i++){
				if (k != i)
				{
					dataGridView1.Rows[i].Cells["Secili"].Value = false;
				}
			}*/
			int k = e.RowIndex;
			
				sayac++;
			if (e.ColumnIndex == dataGridView1.Columns["Secili"].Index){	
				dataGridView1.EndEdit(); //hucreyi editlemeyi sonlandırma
				if ((bool)dataGridView1.Rows[e.RowIndex].Cells["Secili"].Value){					
					String ISBN = dataGridView1.Rows[e.RowIndex].Cells["ISBN"].FormattedValue.ToString();
					tutISBN.Add(ISBN);
					this.dataGridView1.Columns[0].Visible = true;
					
			//		MessageBox.Show(ISBN + "ısbn"/*+dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString()*/); 
				}			
			}
		}

		private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e){

		}

		private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e){
			
			if(dataGridView1.CurrentCell.ColumnIndex==0 && e.Control is ComboBox){
				ComboBox combo = e.Control as ComboBox;
				
				if (combo != null){
					combo.SelectedIndexChanged -= new EventHandler(ComboBox_SelectedIndexChanged);				
				}
				combo.SelectedIndexChanged += new EventHandler(ComboBox_SelectedIndexChanged);

			}
					
					}

   public void ComboBox_SelectedIndexChanged(object gonderilen,EventArgs e){
			ComboBox combo = gonderilen as ComboBox;
			
			if (combo.SelectedItem.ToString() != combo.Items[0]) //bosluk secılmedıyse ekle
{
				tutpuan.Add(combo.SelectedItem.ToString());
				this.dataGridView1.Columns[0].Visible = false;
			//	MessageBox.Show(combo.SelectedItem.ToString() + "++");
			}
		}

		private void btnDevam_Click(object sender, EventArgs e){
		
			if (tutpuan.Count >= 10){
				//--------------------
				ekleKullanici();
				ekleDB();
				MessageBox.Show("Oylama İslemi Tamamlandi!");
			}
			else{
				MessageBox.Show("En az 10 Kitap Oylamalisiniz!");
			}

		}

		////******************************************************************************

		public void ekleDB()
		{
			int kullanID = dondurUserID();
			try
			{
				for (int i = 0; i < Form2.tutpuan.Count; i++)
				{
					baglan.Open();
					
					String sorgu = "insert into bxbookratings values(?,?,?)";

					MySqlCommand komut = new MySqlCommand(sorgu, baglan);
					komut.Parameters.Add("?UserID", kullanID); //select count(*) from bx-users ile al
					komut.Parameters.Add("?ISBN", tutISBN[i]);
					komut.Parameters.Add("?BookRating", tutpuan[i]);

					MySqlDataReader MyReader2;

					
					MyReader2 = komut.ExecuteReader();

					baglan.Close();		
				}
				MessageBox.Show("Uyelik İsleminiz Tamamlandi!");
				/*this.Hide();
				Giris giris = new Giris();
				giris.Show();*/


				Sayfalama sy = new Sayfalama();
				
				this.Hide();
				sy.ShowDialog();
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Can not open connection ! ");
				throw;
			}
		}//ekleDB sonu

		public  int dondurUserID(){
			try{
				baglan.Open();

				String sorgu = "SELECT count(*) FROM yazlab.`bx-users`";
				MySqlCommand komut = new MySqlCommand(sorgu, baglan);

				int userID = Int32.Parse(komut.ExecuteScalar().ToString());
				baglan.Close();

				return userID;
			}
			catch (MySqlException e){

				throw;
			}
		}

		

		private void Form1_Load(object sender, EventArgs e)
		{

		}

		public void listeleRastgele()
		{

		}

		/*	private void button2_Click(object sender, EventArgs e)
			{

				int tutID = getUserID();
				try
				{
					baglan.Open();
					MessageBox.Show("Connection Open ! ");
					String sorgu = "insert into `bx-users` values(?,?,?,?,?)";

					MySqlCommand MyCommand2 = new MySqlCommand(sorgu, baglan);
					MyCommand2.Parameters.Add("?userID", tutID + 1);
					MyCommand2.Parameters.Add("?location", textBox2.Text);
					MyCommand2.Parameters.Add("?age", textBox3.Text);
					MyCommand2.Parameters.Add("?username", textBox4.Text);
					MyCommand2.Parameters.Add("?password", textBox5.Text);
					MySqlDataReader MyReader2;


					MyReader2 = MyCommand2.ExecuteReader();

					baglan.Close();
				}
				catch (MySqlException ex)
				{
					MessageBox.Show("Can not open connection ! ");
					throw;
				}

			}*/

		public void ekleKullanici(){
			Form1 f1 = new Form1();
			int tutID = f1.getUserID();
			try
			{
				baglan.Open();
	//			MessageBox.Show("Connection Open ! ");
				String sorgu = "insert into `bx-users` values(?,?,?,?,?)";

				MySqlCommand MyCommand2 = new MySqlCommand(sorgu, baglan);
				MyCommand2.Parameters.Add("?userID", tutID + 1);
				MyCommand2.Parameters.Add("?location",Form1.t2);
				MyCommand2.Parameters.Add("?age", Form1.t3);
				MyCommand2.Parameters.Add("?username", Form1.t4);
				MyCommand2.Parameters.Add("?password", Form1.t5);
				MySqlDataReader MyReader2;


				MyReader2 = MyCommand2.ExecuteReader();

				baglan.Close();
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Can not open connection ! ");
				throw;
			}
		}

		public int kontrolUnique1(String kullaniciAdi){
			try{
				baglan.Open();

				String sorgu = "SELECT count(*) FROM yazlab.`bx-users` where username=?";
				MySqlCommand komut = new MySqlCommand(sorgu, baglan);
				komut.Parameters.Add("?username", kullaniciAdi);
		//		komut.Parameters.Add("?username", sifre);

				int donenSonuc = Int32.Parse(komut.ExecuteScalar().ToString());//aynı kullanıcı adı ve sıfreyle donen kayıt var mı kontrolu
				baglan.Close();

				return donenSonuc;
			}
			catch (MySqlException e)
			{

				throw;
			}
		}

		public int kontrolUnique2(String sifre){
			try
			{
				baglan.Open();

				String sorgu = "SELECT count(*) FROM yazlab.`bx-users` where password=?";
				MySqlCommand komut = new MySqlCommand(sorgu, baglan);
				//komut.Parameters.Add("?username", kullaniciAdi);
					komut.Parameters.Add("?username", sifre);

				int donenSonuc = Int32.Parse(komut.ExecuteScalar().ToString());//aynı kullanıcı adı ve sıfreyle donen kayıt var mı kontrolu
				baglan.Close();

				return donenSonuc;
			}
			catch (MySqlException e)
			{

				throw;
			}
		}
	}
}
