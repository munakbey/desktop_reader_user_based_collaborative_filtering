﻿using MySql.Data.MySqlClient;
using PdfiumViewer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace yazlab1
{
	public partial class Sayfalama : Form
	{
		public static string cnn = ConfigurationManager.ConnectionStrings["MySql"].ToString();
		public static MySqlConnection baglan = new MySqlConnection(cnn);
		static int bulunulan = 0;
		static int toplamSayfa;
		static int istenen; //istenilen sayfaya gitme
							//	static int suan; //textboxdaki degeri belirleme
		public static string gonderAdres;
		public static string tutYazar;
		public static ArrayList ISBN = new ArrayList();
		int sayac = 0;

		//		ArrayList deneme = new ArrayList();
		public static ArrayList BookTitle = new ArrayList();
		public static ArrayList BookAuthor = new ArrayList();
		public static ArrayList ısbn_sonuc = new ArrayList();

		public static ArrayList tut = new ArrayList();
		public static ArrayList dene = new ArrayList();
		String username;
		public ArrayList url = new ArrayList();

		public static int userID;//okunan kitapları bxbookratings'e eklemek için 
		

		public Sayfalama()
		{
			InitializeComponent();

			/*	dataGridView1.AutoGenerateColumns = false;
				dataGridView1.Columns.Add("ISBN", "ISBN");
				dataGridView1.Columns.Add("BookTitle", "BookTitle");
				dataGridView1.Columns.Add("ImageURLS", "ImageURLS");*/

			getToplamSayfa();
			toplam.Text = ((toplamSayfa / 10) + 1).ToString();

			getURL();

			tut.Add(pictureBox1);
			tut.Add(pictureBox2);
			tut.Add(pictureBox3);
			tut.Add(pictureBox4);
			tut.Add(pictureBox5);
			tut.Add(pictureBox6);
			tut.Add(pictureBox7);
			tut.Add(pictureBox8);
			tut.Add(pictureBox9);
			tut.Add(pictureBox10);



			listele();
			

		}

		private void Sayfalama_Load(object sender, EventArgs e)
		{

		}

		private void dataGridViewPaging1_Load(object sender, EventArgs e)
		{

		}

		public void listele(int suanki = 0)
		{
			suan.Text = ((bulunulan / 10) + 1).ToString();
			try
			{
				baglan.Open();

				MySqlCommand sorgu = new MySqlCommand("SELECT ISBN,BookTitle,BookAuthor,YearOfPublication,Publisher,ImageURLS FROM `bx-books` limit " + suanki + "," + 10, baglan);

				MySqlDataAdapter mda = new MySqlDataAdapter(sorgu);

				DataTable dt = new DataTable();
				dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
				dataGridView1.RowTemplate.Height = 60;

				dataGridView1.AllowUserToAddRows = false;

				mda.Fill(dt);

				dataGridView1.DataSource = dt;

				int j = 0;
				sayac = 1;
				for (int i = suanki; i < suanki + 10; i++)
				{

					if (!(i + 1 > (url.Count)))
					{

						///		MessageBox.Show(i.ToString() + "-----" + (sayac - 1) + ":sayac-1" + url.Count.ToString() + ":url count");
						WebRequest request = WebRequest.Create(url[i].ToString()/*dataGridView1[6, x].Value.ToString()*/);

						using (var response = request.GetResponse())
						{
							using (var str = response.GetResponseStream())
							{
								((PictureBox)tut[j]).Image = Bitmap.FromStream(str);
							}
						}
					}

					else
					{
						//			MessageBox.Show(i.ToString() + " =i\n" + url.Count.ToString() + " :url count");
						((PictureBox)tut[j]).Image = Image.FromFile("C:\\Users\\user\\Desktop\\8.png");
					}
					j++;
				}
				j = 0;



				baglan.Close();
				//		bulunulan += 10;

			}
			catch (MySqlException exc)
			{
				MessageBox.Show("HATA! \nBaglanti Sorunu!");
				throw;
			}

		}

		private void sonraki_Click(object sender, EventArgs e)
		{
			bulunulan += 10;
			listele(bulunulan);

		}

		private void onceki_Click(object sender, EventArgs e)
		{
			bulunulan -= 10;
			//		MessageBox.Show((bulunulan ).ToString());
			listele((bulunulan));
		}

		public int getToplamSayfa()
		{

			try
			{
				baglan.Open();
				String sorgu = "SELECT count(*) FROM yazlab.`bx-books`";
				MySqlCommand komut = new MySqlCommand(sorgu, baglan);
				toplamSayfa = Int32.Parse(komut.ExecuteScalar().ToString());
				baglan.Close();
				return toplamSayfa;
			}
			catch (MySqlException e)
			{
				throw;
			}
		}

		private void git_Click(object sender, EventArgs e)
		{
			istenen = Int32.Parse(suan.Text);
			int kullan_istenen = (Int32.Parse(suan.Text) * 10) - 10; ;
			//	MessageBox.Show(istenen.ToString());

			listele(kullan_istenen);
			suan.Text = istenen.ToString();
			bulunulan = kullan_istenen; //gittimiz sayfadan sonra ileri-geri islemlerine devam etmek için
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Pdf pdf = new Pdf(1,1);
			pdf.Show();
		}



		public void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
		{

			if (e.RowIndex > -1)
			{
				gonderAdres = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
				MessageBox.Show(gonderAdres+"??");
				tutYazar = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
				//	ekleOkunan();//acacagı kitabı okunanlar tablosuna ekler


					getUserID();
					okunanEkleme();
				

				Pdf pdf = new Pdf(1,1);
				pdf.Show();
			
				//		MessageBox.Show(tutYazar);

			}
		}

		/*	public void ekleOkunan(){

				try
				{
					baglan.Open();
					if (Giris.username != null)
					{
						username = Giris.username;
					}
					else
					{
						username = Form1.t4;
					}
					String sorgu = "insert into `okunan` values(?,?,?)";

					MySqlCommand MyCommand2 = new MySqlCommand(sorgu, baglan);
					MyCommand2.Parameters.Add("?username", username);
					MyCommand2.Parameters.Add("?ISBN", gonderAdres);
					MyCommand2.Parameters.Add("?BookAuthor", tutYazar);

					MySqlDataReader MyReader2;


					MyReader2 = MyCommand2.ExecuteReader();

					baglan.Close();
				}
				catch (MySqlException ex)
				{
					MessageBox.Show("Can not open connection ! ");
					throw;
				}
			}*/

		private void oneri_Click(object sender, EventArgs e)
		{

			/*	tutISBN();
				if (Giris.username != null){
					username = Giris.username;

				}
				else{
					username = Form1.t4;

				}

				if (Int32.Parse(getOkunanKitapSayisi(username).ToString()) == 0)
				{
					MessageBox.Show("EN AZ BİR KİTAP OKUMALISINIZ!!");
				}
				else
				{
					getOnerilen();
				}*/
		}

		/*	public int getOkunanKitapSayisi(string username){
				try{
					baglan.Open();
					if (Giris.username != null){
						username = Giris.username;

					}
					else{
						username = Form1.t4;
					}
					String sorgu = "SELECT count(*) FROM yazlab.`okunan` where username=?";

					MySqlCommand komut = new MySqlCommand(sorgu, baglan);
					komut.Parameters.Add("?username", username);
					toplamSayfa = Int32.Parse(komut.ExecuteScalar().ToString());
					baglan.Close();
					return toplamSayfa;
				}
				catch (MySqlException e){
					throw;
				}
			}*/

		/*public void tutISBN(){
			baglan.Open();
			if (Giris.username != null){
				username = Giris.username;
			}
			else{
				username = Form1.t4;
			}
			String sorgu = "SELECT ISBN FROM okunan where username='" + username + "'";

			MySqlCommand MyCommand2 = new MySqlCommand(sorgu, baglan);

			MySqlDataReader Rs = MyCommand2.ExecuteReader();

			while (Rs.Read()){

				ISBN.Add(Rs["ISBN"].ToString());
			MessageBox.Show(Rs["ISBN"].ToString()+"ISBN***22222255");
			}
			baglan.Close();
		}*/

		/*    public void getOnerilen(){
					BookTitle.Clear();
					BookAuthor.Clear();
					ısbn_sonuc.Clear();

				if (Giris.username != null){
						username = Giris.username;

					}
					else{
						username = Form1.t4;

					}

					int tmp = Int32.Parse(getOkunanKitapSayisi(username).ToString());

					try{

						for (int i = 0; i < Int32.Parse(getOkunanKitapSayisi(username).ToString()); i++){
							baglan.Open();
							String sorgu = "SELECT BookTitle,BookAuthor,ISBN FROM `bx-books` where `bx-books`.BookAuthor = (" +
								" select okunan.BookAuthor from okunan inner join `bx-books` on `bx-books`.ISBN = okunan.ISBN where okunan.username =? and okunan.ISBN =? " +
				 ") AND `bx-books`.ISBN !=?";

							MySqlCommand MyCommand2 = new MySqlCommand(sorgu, baglan);

							MessageBox.Show(i.ToString() + "   =i");
							MyCommand2.Parameters.Add("?okunan.username", username);
							MyCommand2.Parameters.Add("?okunan.ISBN", ISBN[i]);
							MyCommand2.Parameters.Add("?`bx-books`.ISBN", ISBN[i]);



							MySqlDataReader Rs = MyCommand2.ExecuteReader();
							System.Data.DataTable dt = new System.Data.DataTable();


							while (Rs.Read())
							{
								BookTitle.Add(Rs["BookTitle"].ToString());
								BookAuthor.Add(Rs["BookAuthor"].ToString());
								ısbn_sonuc.Add(Rs["ISBN"].ToString());
								MessageBox.Show(Rs["BookTitle"].ToString() + "!!"+ISBN[i].ToString());
							}


							baglan.Close();
						}

					}
					catch (MySqlException ex)
					{
						MessageBox.Show("Can not open connection ! ");
						throw;
					}



					Listeleme list = new Listeleme();
					list.gosterliste();
				   list.Show();




			}*/

		private void button1_Click_1(object sender, EventArgs e)
		{
			enler_listele list = new enler_listele();

			string en_iyi_kitaplar = "select ISBN,avg(BookRating) from bxbookratings group by ISBN order by avg(BookRating) desc limit 10";
			list.listele(en_iyi_kitaplar);
			list.ShowDialog();
		}

		private void button2_Click(object sender, EventArgs e){
			enler_listele list = new enler_listele();

			string en_iyi_kitaplar = "select ISBN,count(*) from bxbookratings group by ISBN order by count(*) desc limit 11";
			//select ISBN,count(*) from bxbookratings  where bxbookratings.BookRating is not null group by ISBN order by count(*)  desc limit 11   OLMALİ!!!
			list.listele(en_iyi_kitaplar);
			list.ShowDialog();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			enler_listele list = new enler_listele();
			int toplamkitap = getToplamSayfa();
			string enyeniler;

			enyeniler = "select ISBN,BookTitle,BookAuthor,YearOfPublication,Publisher from `bx-books` limit " + (toplamkitap - 5) + ",5";

			/*	if (toplamkitap > 271384){
					enyeniler = "select ISBN,BookTitle,BookAuthor,YearOfPublication,Publisher from `bx-books` limit " + (toplamkitap - 5) + ",5";
				}
				else if(toplamkitap== 271379){
					enyeniler = "select ISBN,BookTitle,BookAuthor,YearOfPublication,Publisher from `bx-books` limit  271374,5";
				}
				else{
					enyeniler = "select ISBN,BookTitle,BookAuthor,YearOfPublication,Publisher from `bx-books` limit  271379,5";
				}*/
			list.listele(enyeniler);
			list.ShowDialog();
		}

		private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e){

		}

		private void button4_Click(object sender, EventArgs e){
			Filtreleme flt = new Filtreleme();
			flt.ShowDialog();
			this.Hide();
		}


		public void getURL(){

			baglan.Open();

			try{
				String sorgu = "SELECT ImageURLS FROM `bx-books` ";
				MySqlCommand komut = new MySqlCommand(sorgu, baglan);
				MySqlDataReader Rs = komut.ExecuteReader();

				while (Rs.Read()){
					url.Add(Rs["ImageURLS"].ToString());
				}
				baglan.Close();
			}
			catch (MySqlException ex){
				MessageBox.Show("Baglanti Hatasi");
			}

		}

		public void okunanEkleme(){

			try{
				baglan.Open();

				//	String sorgu = "insert into `bxbookratings`(UserID,ISBN) values(?,?)";
				String sorgu = "INSERT INTO `bxbookratings` (`UserID`, `ISBN`) SELECT " + userID + ",'" + gonderAdres + "' FROM dual WHERE NOT EXISTS(select UserID, ISBN from bxbookratings where UserID = " + userID + " and ISBN = '" + gonderAdres + "')";
				MySqlCommand MyCommand2 = new MySqlCommand(sorgu, baglan);
		/*		MyCommand2.Parameters.Add("?UserID", userID);
				MessageBox.Show(gonderAdres + "==");
				MyCommand2.Parameters.Add("?ISBN", gonderAdres);*/
				

				MySqlDataReader MyReader2;


				MyReader2 = MyCommand2.ExecuteReader();

				baglan.Close();
			}
			catch (MySqlException ex){
				MessageBox.Show("Can not open connection ! ");
				throw;
			}
		}

		public static int getUserID(){
			try{
				baglan.Open();
				String sorgu = "select userID  from `bx-users` where username= '"+Giris.username+"'";
				MySqlCommand komut = new MySqlCommand(sorgu, baglan);
				userID = Int32.Parse(komut.ExecuteScalar().ToString());

				baglan.Close();
			

			}
			catch (MySqlException e){
				MessageBox.Show("getUserID-Baglanti Hatasi!");
				throw;
			}
			return userID;
		}


		
	}
}