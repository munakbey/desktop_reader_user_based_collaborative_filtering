﻿namespace yazlab1
{
	partial class Pdf
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.helpProvider1 = new System.Windows.Forms.HelpProvider();
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.ileri = new System.Windows.Forms.Button();
			this.geri = new System.Windows.Forms.Button();
			this.toplam = new System.Windows.Forms.TextBox();
			this.suanki = new System.Windows.Forms.TextBox();
			this.git = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// richTextBox1
			// 
			this.richTextBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.richTextBox1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
			this.richTextBox1.Location = new System.Drawing.Point(12, 12);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.ReadOnly = true;
			this.richTextBox1.Size = new System.Drawing.Size(659, 316);
			this.richTextBox1.TabIndex = 3;
			this.richTextBox1.Text = "";
			// 
			// ileri
			// 
			this.ileri.Location = new System.Drawing.Point(382, 359);
			this.ileri.Name = "ileri";
			this.ileri.Size = new System.Drawing.Size(75, 23);
			this.ileri.TabIndex = 4;
			this.ileri.Text = "İleri";
			this.ileri.UseVisualStyleBackColor = true;
			this.ileri.Click += new System.EventHandler(this.ileri_Click);
			// 
			// geri
			// 
			this.geri.Location = new System.Drawing.Point(108, 359);
			this.geri.Name = "geri";
			this.geri.Size = new System.Drawing.Size(75, 23);
			this.geri.TabIndex = 5;
			this.geri.Text = "Geri";
			this.geri.UseVisualStyleBackColor = true;
			this.geri.Click += new System.EventHandler(this.geri_Click);
			// 
			// toplam
			// 
			this.toplam.Location = new System.Drawing.Point(294, 361);
			this.toplam.Name = "toplam";
			this.toplam.Size = new System.Drawing.Size(57, 20);
			this.toplam.TabIndex = 6;
			// 
			// suanki
			// 
			this.suanki.Location = new System.Drawing.Point(202, 362);
			this.suanki.Name = "suanki";
			this.suanki.Size = new System.Drawing.Size(68, 20);
			this.suanki.TabIndex = 7;
			// 
			// git
			// 
			this.git.Location = new System.Drawing.Point(253, 397);
			this.git.Name = "git";
			this.git.Size = new System.Drawing.Size(75, 23);
			this.git.TabIndex = 8;
			this.git.Text = "Git";
			this.git.UseVisualStyleBackColor = true;
			this.git.Click += new System.EventHandler(this.git_Click);
			// 
			// Pdf
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(687, 450);
			this.Controls.Add(this.git);
			this.Controls.Add(this.suanki);
			this.Controls.Add(this.toplam);
			this.Controls.Add(this.geri);
			this.Controls.Add(this.ileri);
			this.Controls.Add(this.richTextBox1);
			this.Name = "Pdf";
			this.Text = "Pdf";
			this.Load += new System.EventHandler(this.Pdf_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.HelpProvider helpProvider1;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private System.Windows.Forms.Button ileri;
		private System.Windows.Forms.Button geri;
		private System.Windows.Forms.TextBox toplam;
		private System.Windows.Forms.TextBox suanki;
		private System.Windows.Forms.Button git;
	}
}