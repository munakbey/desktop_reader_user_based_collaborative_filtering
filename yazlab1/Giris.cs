﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace yazlab1
{
	public partial class Giris : Form
	{
		public static string cnn = ConfigurationManager.ConnectionStrings["MySql"].ToString();
		public MySqlConnection baglan =new  MySqlConnection(cnn);
		public static string username;

		public Giris()
		{
			InitializeComponent();
			textBox2.PasswordChar = '●';
			
		}

		private void Giris_Load(object sender, EventArgs e)
		{

		}

		public void girisKontrol()
		{
			try{

			baglan.Open();

				MySqlCommand sorgu = new MySqlCommand("Select count(*) from `bx-users` where username=? and password=?",baglan);
			//	MessageBox.Show("Select count(*) from `bx-users` where username='" + kA.Text + "' and password='" + sifre.Text + "'".ToString());
				sorgu.Parameters.Add("?username", kA.Text);
				sorgu.Parameters.Add("?password", textBox2.Text);

		int donenDeger = Int32.Parse(sorgu.ExecuteScalar().ToString());
	
				if (donenDeger == 1){
					this.Hide();
					MessageBox.Show("Giris Yapildi!");
					Sayfalama syf = new Sayfalama();
					syf.ShowDialog();
				}
				else{
					MessageBox.Show("Giris Yapilamadi!\nKullanici Adi ve Sifrenizi Kontrol Ediniz!");
				}
				
				baglan.Close();
			}
			catch (MySqlException exc)
			{
				MessageBox.Show("ttttttttt");
				throw;
			}
		}

		private void btn_Giris_Click(object sender, EventArgs e)
		{
			
			username = kA.Text.ToString();
			girisKontrol();
		}

		private void uyeOl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			this.Hide();
			Form1 f1 = new Form1();
			f1.ShowDialog();
		}

		private void adminGiris_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			adminpanelgiris apg = new adminpanelgiris();
			apg.Show();
			this.Hide();
		}
	}
}
