﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Font = System.Drawing.Font;

namespace yazlab1
{
	public partial class Pdf : Form
	{
		public static string cnn = ConfigurationManager.ConnectionStrings["MySql"].ToString();
		public MySqlConnection baglan = new MySqlConnection(cnn);

		public  int bas=1, son=1;
		public Pdf(int bas,int son/*,string pdf=null*/)
		{
			InitializeComponent();
			sayfala(bas,son);
			getAdres(/*pdf*/);
			
		}
		public Pdf(int bas, int son, string pdf )
		{
			InitializeComponent();
			sayfala(bas, son);
			getAdres(pdf);

		}


		private void pdfViewer1_Load(object sender, EventArgs e)
		{

		}

		private void Pdf_Load(object sender, EventArgs e)
		{

		}

		private void button1_Click(object sender, EventArgs e){

			/*
						try {
							iTextSharp.text.pdf.PdfReader reader = new iTextSharp.text.pdf.PdfReader(@"C:\Users\user\Desktop\a.pdf");

							StringBuilder sb = new StringBuilder();
							for (int i = 1; i <=1; i++) {
				
								sb.Append(PdfTextExtractor.GetTextFromPage(reader, i));
							}
							richTextBox1.Text = sb.ToString();
				toplam.Text = reader.NumberOfPages.ToString();
				reader.Close();

						}
						catch (Exception ex)
						{

						}
		
		*/

		}

		private void ileri_Click(object sender, EventArgs e)
		{
			bas++;
			son++;
			sayfala(bas, son);
		}

		private void geri_Click(object sender, EventArgs e)
		{
			son--;
			bas--;
			sayfala(bas, son);
		}

		private void git_Click(object sender, EventArgs e)
		{
			int girilen = Int32.Parse(suanki.Text);
			bas = girilen;
			son = girilen;
			sayfala(girilen, girilen);
		}

		public void sayfala(int x, int y){


			try{
				iTextSharp.text.pdf.PdfReader reader = new iTextSharp.text.pdf.PdfReader(@getAdres());
				BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

				StringBuilder sb = new StringBuilder();
				for (int i = x; i <= y; i++){
					//			MessageBox.Show(reader.NumberOfPages.ToString());

					iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 40, iTextSharp.text.Font.BOLD);
					sb.Append(PdfTextExtractor.GetTextFromPage(reader, i));
			//		iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL);
				}
				
				richTextBox1.Text = sb.ToString();
				toplam.Text = reader.NumberOfPages.ToString();
				suanki.Text = bas.ToString();

				reader.Close();
			}
			catch (Exception ex){
			}
	//		MessageBox.Show(x.ToString() + "*********" + y.ToString());
		}

		public  string getAdres(string dondur=null){
		//	String dondur;
			try
			{
				baglan.Open();
		//		MessageBox.Show("Connection Open ! \n" );
				String sorgu = "select pdf from `bx-pdf` where ISBN=?";

				MySqlCommand MyCommand2 = new MySqlCommand(sorgu, baglan);
				MyCommand2.Parameters.Add("?ISBN", Sayfalama.gonderAdres);

				MySqlDataReader MyReader2;



				//	MyReader2 = MyCommand2.ExecuteReader();
				
				 dondur = MyCommand2.ExecuteScalar().ToString();

				baglan.Close();
			//	return dondur.ToString();

			}
			catch (MySqlException ex){
				MessageBox.Show("Can not open connection ! ");
				throw;
			}
			

			return dondur.ToString();
		}



	}
	
}
