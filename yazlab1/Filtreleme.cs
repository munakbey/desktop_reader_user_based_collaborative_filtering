﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace yazlab1
{
	public partial class Filtreleme : Form
	{
		public static string cnn = ConfigurationManager.ConnectionStrings["MySql"].ToString();
		public MySqlConnection baglan = new MySqlConnection(cnn);
		public int kullaniciSayisi;
		int index;            //istenilen kullanıcının bulundugu indexi dondurme.
		public int[,] matris;//Yakınlıkların tutulacağı matris
		public ArrayList tutOkunan = new ArrayList();//aktif kullanıcının okudugu kıtapların ISBN sini tutar
		public ArrayList tutOylar = new ArrayList();//aktif kullanıcının verdiği oyları tutar.
		public ArrayList tutID = new ArrayList();
		public ArrayList nullOlmayanlar = new ArrayList();
		public Dictionary<int, int> total = new Dictionary<int, int>(); //key userID value=yakınlık degeri
		public Dictionary<int, int> sirali = new Dictionary<int, int>();

		public static int userid; //aktif kullanicinin ID'si.
		public static string loc;//aktif kullanicinin location bilgisi.
		public static int yas;//aktif kullanicinin yas bilgisi.

		
		public int maxYakinlik;

		public static ArrayList tutGecici = new ArrayList();//onerilecek kitap ISBN lerini gecici olarak tutar.İsmiyle beraber listeletmek için.
		public static ArrayList BookTitle = new ArrayList();
		public static ArrayList BookAuthor = new ArrayList();
		public static ArrayList ısbn_ = new ArrayList();

		public static string pdf;//gosterilecek pdf in yolunu tutar.
		public static string ısbn;//gosterilecek kitabın ısbnsini tutar.


		public Filtreleme()
		{
			InitializeComponent();
			alKullaniciBilgisi(); //aktif kullanicinin bilgilerini alıyoruz.

			SayKullanici();  //Kıyaslayacağımız kullanıcı sayısını tutuyoruz-kullaniciSayisi-;




			matris = new int[kullaniciSayisi, 5];//5: userID-age-location-okunana gore yakınlık-oy vermeye gore yakınlık
			belirleKullanici(); //0. kolon (userID)

			kiyaslaYas(yas); //1. kolon (yas)

			kiyaslaYer(loc); //2. kolon(location)
			ataDefault(3); //okunan yakınlık oncesinde tum degerlerı 0 yapma

			okunanKitaplar(userid);

			okuyanKullanicilar();
			ataDefault(4);
			oyYakinlik();
			toplamBul();

	//		MessageBox.Show(userid.ToString() + "**" + loc.ToString() + "**" + yas.ToString());
			oneri();
			//		gosterOneri();

			doldurArrayList();

			dataGridView1.ColumnCount = 3;
			dataGridView1.Columns[0].Name = "ISBN";
			dataGridView1.Columns[1].Name = "Book Title";
			dataGridView1.Columns[2].Name = "Book Author";

			dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

			for (int i = 0; i < BookTitle.Count; i++){
				//		MessageBox.Show(Sayfalama.sorguSonucu[i].ToString(),);
				dataGridView1.Rows.Add(/*tutGecici[i],*/ısbn_[i],BookTitle[i],BookAuthor[i]);


			}


		}

		private void Filtreleme_Load(object sender, EventArgs e)
		{

		}

		public int SayKullanici()
		{
			try
			{
				baglan.Open();
				String sorgu = "select count(distinct UserID)  from `bxbookratings` ";
				MySqlCommand komut = new MySqlCommand(sorgu, baglan);

				kullaniciSayisi = Int32.Parse(komut.ExecuteScalar().ToString());

				baglan.Close();

				return kullaniciSayisi;

				baglan.Close();
			}
			catch (MySqlException e)
			{
				MessageBox.Show("SayKullanici-Baglanti Hatasi!");
				throw;
			}
		}

		public void belirleKullanici()
		{
			int i = 0; baglan.Open();
			/*	try
				{
					*/
			String sorgu = "select distinct UserID  from `bxbookratings`";
			MySqlCommand MyCommand2 = new MySqlCommand(sorgu, baglan);
			MySqlDataReader Rs = MyCommand2.ExecuteReader();

			while (Rs.Read())
			{
				matris[i, 0] = Int32.Parse(Rs["UserID"].ToString());
				i++;
			}
			baglan.Close();
			/*	}
				catch (MySqlException ex){

				}*/



		}

		public void kiyaslaYas(int aktifKullaniciYasi)
		{
			int x; // herbir user ın matristeki indexini tutar.
			try
			{
				baglan.Open();
				String sorgu = "select distinct `bx-users`.UserID,age  from `bxbookratings`,`bx-users` where  `bxbookratings`.UserID=`bx-users`.userID and age=" + aktifKullaniciYasi;
				MySqlCommand komut = new MySqlCommand(sorgu, baglan);
				MySqlDataReader Rs = komut.ExecuteReader();

				while (Rs.Read())
				{

					x = bulIndex(Int32.Parse(Rs["UserID"].ToString())); //herbir user ın matristeki indexini bulur.					
					matris[x, 1] = 1;

				}
				baglan.Close();
				doldurBosluk(1);//age kolonu(1)

			}
			catch (MySqlException ex)
			{
			}
		}

		public void kiyaslaYer(string aktifKullaniciYer)
		{
			int x; // herbir user ın matristeki indexini tutar.
			try
			{
				baglan.Open();
				String sorgu = "select distinct `bx-users`.UserID,location  from `bxbookratings`,`bx-users` where  `bxbookratings`.UserID=`bx-users`.userID and location like '%" + aktifKullaniciYer + "%'";
				MySqlCommand komut = new MySqlCommand(sorgu, baglan);
				MySqlDataReader Rs = komut.ExecuteReader();

				while (Rs.Read())
				{

					x = bulIndex(Int32.Parse(Rs["UserID"].ToString())); //herbir user ın matristeki indexini bulur.					
					matris[x, 2] = 1;

				}
				baglan.Close();
				doldurBosluk(2);//location kolonu(2)

			}
			catch (MySqlException ex)
			{
			}
		}

		public int bulIndex(int ID)
		{

			for (int i = 0; i < matris.Length; i++)
			{
				if (matris[i, 0] == ID)
				{
					index = i;
					break;
				}
			}
			return index;
		}

		public void doldurBosluk(int sutun)
		{
			for (int i = 0; i < kullaniciSayisi; i++)
			{
				if (matris[i, sutun] != 1)
				{
					matris[i, sutun] = 0;
				}
			}
		}

		public void ataDefault(int kolon)
		{
			for (int i = 0; i < kullaniciSayisi; i++)
			{
				matris[i, kolon] = 0;
			}
		}

		public void okunanKitaplar(int aktifID)
		{
			int x;
			try
			{
				baglan.Open();
				String sorgu = "SELECT ISBN,BookRating FROM yazlab.bxbookratings where UserID=" + aktifID;
				MySqlCommand MyCommand2 = new MySqlCommand(sorgu, baglan);
				MySqlDataReader Rs = MyCommand2.ExecuteReader();

				while (Rs.Read())
				{
					tutOkunan.Add(Rs["ISBN"].ToString());

				}
				baglan.Close();

				baglan.Open();
				String sorgu2 = "SELECT ISBN,BookRating FROM yazlab.bxbookratings where UserID=" + aktifID + " and BookRating is not null";
				MySqlCommand MyCommand = new MySqlCommand(sorgu2, baglan);
				MySqlDataReader Rs2 = MyCommand.ExecuteReader();

				while (Rs2.Read())
				{
					nullOlmayanlar.Add(Rs2["ISBN"].ToString());
					tutOylar.Add(Rs2["BookRating"].ToString());
				}
				baglan.Close();
			}
			catch (MySqlException ex)
			{
			}
		}


		public void okuyanKullanicilar()
		{
			int x;
			try
			{

				for (int i = 0; i < tutOkunan.Count; i++)
				{
					baglan.Open();
					String sorgu = "SELECT ISBN,UserID FROM yazlab.bxbookratings where ISBN='" + tutOkunan[i] + "'";

					//		String sorgu = "select userID from bxbookratings where ISBN='0195153448' ";

					MySqlCommand MyCommand2 = new MySqlCommand(sorgu, baglan);

					MySqlDataReader Rs = MyCommand2.ExecuteReader();
					while (Rs.Read())
					{
						tutID.Add(Rs["UserID"].ToString());
					}

					for (int j = 0; j < tutID.Count; j++)
					{
						x = bulIndex(Int32.Parse(tutID[j].ToString())); //herbir user ın matristeki indexini bulur.					              
						matris[x, 3] += 1;
						//               MessageBox.Show(matris[x, 3].ToString() + "****"+x.ToString()+"---"+ matris[x, 0]);
					}

					tutID.Clear();
					x = 0;

					baglan.Close();
				}

			}
			catch (MySqlException ex)
			{
			}
		}

		public void oyYakinlik()
		{
			int x;
			ArrayList tmp = new ArrayList(); // yakınlıklıklarına bakcagımız userların oylarını tutar.

			try
			{

				for (int i = 0; i < nullOlmayanlar.Count; i++)
				{
					baglan.Open();
					String sorgu = "SELECT UserID,BookRating FROM yazlab.bxbookratings where ISBN='" + nullOlmayanlar[i] + "' and BookRating is not null";

					MySqlCommand MyCommand2 = new MySqlCommand(sorgu, baglan);

					MySqlDataReader Rs = MyCommand2.ExecuteReader();
					while (Rs.Read())
					{

						tutID.Add(Rs["UserID"].ToString());
						tmp.Add(Int32.Parse(Rs["BookRating"].ToString())); //verilen oyu geçici olarak alıyoruz.
																		   //				MessageBox.Show(Int32.Parse(Rs["BookRating"].ToString())+"////");
					}
					//	       MessageBox.Show(tmp[i].ToString()+"**"+tutOkunan.Count.ToString()+ "-tutOkunan--" + tutOylar.Count.ToString()+ "+++tutOylar+++" + tmp.Count.ToString() + "+++tmp+++" + tutID.Count.ToString());


					for (int j = 0; j < tmp.Count; j++)
					{

						x = bulIndex(Int32.Parse(tutID[j].ToString())); //herbir user ın matristeki indexini bulur.		

						matris[x, 4] += (10 - (Math.Abs(Int32.Parse(tutOylar[i].ToString()) - Int32.Parse(tmp[j].ToString()))));
						//				MessageBox.Show(tutOylar[i].ToString() + " =mtrs"+ tmp[j].ToString());
					}

					//		MessageBox.Show(tmp[i].ToString() + "**" + tutOkunan.Count.ToString() + "-tutOkunan--" + tutOylar.Count.ToString() + "+++tutOylar+++" + tmp.Count.ToString() + "+++tmp+++" + tutID.Count.ToString());

					tutID.Clear();
					tmp.Clear();



					baglan.Close();


				}

			}
			catch (MySqlException ex)
			{
			}
		}
		public void toplamBul()
		{
			int temp;
			for (int i = 0; i < kullaniciSayisi; i++)
			{
				temp = matris[i, 1] + matris[i, 2] + matris[i, 3] + matris[i, 4];
				total.Add(matris[i, 0], temp);
				temp = 0;

			}
			//	MessageBox.Show(matris[0, 4].ToString() + "****"  + "---" + matris[0, 0].ToString());
			sirali = total.OrderByDescending(x => x.Value).ToDictionary(
			   x => x.Key,
			   x => x.Value);
			/*	foreach (var i in sirali){
				MessageBox.Show(i.Key.ToString() + "***" + i.Value.ToString());
				}*/

		}

		public void oneri(){
			int t=0;
				foreach (var i in sirali){
				if (t >= 1 && t < 2){					
					maxYakinlik=Int32.Parse(i.Value.ToString());
					MessageBox.Show(maxYakinlik + "<-max-");
				}
				t++;
			}
			int sayac = 0;
			int x = 0;
			try
			{
				foreach (var i in sirali){
					/*if (sayac >= 1 && sayac < 2)
					{*/
		//			MessageBox.Show(tutGecici.Count.ToString() + "------" + maxYakinlik.ToString());
					if (Int32.Parse(i.Value.ToString()) <= maxYakinlik && tutGecici.Count < 20  /*&& sayac<2*/)
					{
		//				MessageBox.Show(i.Key.ToString() + "<--" + i.Value.ToString());
						baglan.Open();
						String sorgu = "SELECT distinct ISBN FROM yazlab.bxbookratings where UserID=" + i.Key.ToString() + " and ISBN not in " +
							"(select ISBN from `bxbookratings` where UserID=" + userid + ")";
						MySqlCommand MyCommand2 = new MySqlCommand(sorgu, baglan);
						MySqlDataReader Rs = MyCommand2.ExecuteReader();

						while (Rs.Read())
						{
							if (tutGecici.Count < 20)
							{
								tutGecici.Add(Rs["ISBN"].ToString());
								//	MessageBox.Show(Rs["ISBN"].ToString() + "##");
							}
							else
							{
								break;
							}
						}

						/*	MySqlDataAdapter mda = new MySqlDataAdapter();
							mda.SelectCommand = sorgu_;

							DataTable dt = new DataTable();
							mda.Fill(dt);

							dataGridView1.DataSource = dt;*/

						baglan.Close();
						sayac++;
					}
					
				/*	}
					sayac++;*/
				}
			}
			catch (MySqlException exc){
				MessageBox.Show("Veri Tabanına Baglanamadı!! ");
				throw;
			}
		}

		private void label1_Click(object sender, EventArgs e)
		{

		}


		public void alKullaniciBilgisi(){ // giris yapan kullanicinin yas-loc-userID bilgilerini alma

			string[] loc_temp;
			string locTut;
			try{

				baglan.Open();
				MessageBox.Show("acildii");
				String sorgu = "SELECT * FROM yazlab.`bx-users` where username = '" + Giris.username + "'";
				MySqlCommand MyCommand2 = new MySqlCommand(sorgu, baglan);
				MySqlDataReader Rs = MyCommand2.ExecuteReader();

				while (Rs.Read()){

					userid = Int32.Parse(Rs["userID"].ToString());

					locTut = (Rs["location"].ToString());
					loc_temp = locTut.Split(',');
					loc = loc_temp[2];
					MessageBox.Show(loc + "!!!!!!!!!!!");

					if (DBNull.Value.Equals(Rs["age"])){
						//	MessageBox.Show("zaaaaaaa");
						yas = -1;
					}
					else{
						yas = Int32.Parse(Rs["age"].ToString());
					}
				}

				baglan.Close();

			}
			catch (MySqlException ex)
			{
				MessageBox.Show("alKullaniciBilgisi -BAGLANTI HATASI!");
			}
		}

	/*	public void gosterOneri()
		{
			try
			{
				for (int i = 0; i < tutGecici.Count; i++)
				{
					baglan.Open();
					String sorgu = "select * from `bx-books` where ISBN='" + tutGecici[i] + "'";
					MySqlCommand sorgu_ = new MySqlCommand(sorgu, baglan);

					MySqlDataAdapter mda = new MySqlDataAdapter();
					mda.SelectCommand = sorgu_;

					DataTable dt = new DataTable();
					mda.Fill(dt);

					dataGridView1.DataSource = dt;

					baglan.Close();
				}
			}
			catch (MySqlException exc)
			{
				MessageBox.Show("Veri Tabanına Baglanamadı!! ");
				throw;
			}
		}*/

		public void doldurArrayList(){
			try{
				for (int i = 0; i < tutGecici.Count; i++){
					baglan.Open();

					String sorgu = "SELECT distinct * FROM yazlab.`bx-books` where  ISBN='" + tutGecici[i] + "'";
					MySqlCommand MyCommand2 = new MySqlCommand(sorgu, baglan);
					MySqlDataReader Rs = MyCommand2.ExecuteReader();

					while (Rs.Read())
					{
						BookAuthor.Add(Rs["BookAuthor"].ToString());
						BookTitle.Add(Rs["BookTitle"].ToString());
						ısbn_.Add(Rs["ISBN"].ToString());
			//			MessageBox.Show("**");
					}

					baglan.Close();
				}
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("doldurArrayList -BAGLANTI HATASI!");
			}
		}

		private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e){
			
			string tmp=getID_Kitap(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());

			Pdf ac = new Pdf(1, 1);
			ac.getAdres(getPdf(tmp));
			ac.Show();

			int kullanici = Sayfalama.getUserID();
			enler_listele.okunanaEkle(kullanici, tmp);

		}
		public string getID_Kitap(string kitapAdi)
		{//Kitap adından pdf ine ulasmak için ISBN ye geçiş.Ratingdeki kitapların hepsi booksda olmadığından ArrayListten direkt çekilmedi.

			try
			{
				baglan.Open();
				String sorgu = "select ISBN from `bx-books` where BookTitle=?";
				MySqlCommand komut = new MySqlCommand(sorgu, baglan);
				komut.Parameters.Add("?BookTitle", kitapAdi);
				MySqlDataReader Rs = komut.ExecuteReader();

				while (Rs.Read())
				{
					ısbn = Rs["ISBN"].ToString();
				}
				baglan.Close();


			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Baglanti Hatasi");
			}

			return ısbn;
		}

		public string getPdf(string ısbn)
		{
			try
			{

				baglan.Open();
				String sorgu = "select pdf from `bx-pdf` where ISBN=?";
				MySqlCommand komut = new MySqlCommand(sorgu, baglan);
				komut.Parameters.Add("?ISBN", ısbn);
				MySqlDataReader Rs = komut.ExecuteReader();

				while (Rs.Read())
				{
					pdf = (Rs["pdf"].ToString());
				}
				baglan.Close();


			}
			catch (MySqlException ex)
			{
				MessageBox.Show("getPdf-Baglanti Hatasi");
			}
			return pdf;
		}
	}
}